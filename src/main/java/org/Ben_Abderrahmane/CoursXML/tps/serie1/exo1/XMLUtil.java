package org.Ben_Abderrahmane.CoursXML.tps.serie1.exo1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class XMLUtil {

	/**
	 * 
	 * @param marin
	 * @return
	 */
	public static Document serialize(Marin marin){
		Document doc= DocumentHelper.createDocument();
		Element root=doc.addElement("marin").addAttribute("id",String.valueOf(marin.getId())).addAttribute("long","FR");
		Element nom=root.addElement("Nom").addText(marin.getNom());
		Element prenom=root.addElement("Prenom").addText(marin.getPrenom());
		Element age =root.addElement("Age").addText(String.valueOf(marin.getAge()));
	
		return doc;
	}
	
	/**
	 * une methode qui permet de cr�er un fichier
	 * @param nom
	 * @return
	 */
	public static File createNewFile(String nom){
	File file=null;
	String str =nom;
	boolean bool = false;
	try{
		 file = new File(str); 
		 bool = file.exists();  
		 if(bool){ 
			 str = file.toString(); 
			 System.out.println("pathname string: "+str+"\n");
		 }
	}
	catch(Exception e){ 
		e.printStackTrace(); 
		 }
	return file;
	}
	
	/**
	 * 
	 * @param doc
	 * @param file
	 */
	public static void write(Document doc,File file){
		OutputFormat format= OutputFormat.createPrettyPrint();
		try{
		OutputStream fos=new FileOutputStream(file);
		XMLWriter writer=new XMLWriter(fos,format);
		writer.write(doc);
		writer.close();
		}
		catch (IOException e){
			// affichage du message d�erreur et de la pile d�appel 
			System.out.println("Erreur " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param file
	 * @return
	 * @throws DocumentException
	 */
	public static Document read(File file) throws DocumentException{
		SAXReader xmlReader =  new SAXReader() ;
		Document doc = xmlReader.read(file) ;
		return doc;
	}
	
	/**
	 * 
	 * @param doc
	 * @return
	 */
	public static Marin deserialize(Document doc){
		
		Element root=doc.getRootElement();
	
		List attributeliste=root.attributes();
		Attribute attr=(Attribute) attributeliste.get(0);
		String name =attr.getName();
		String idstr =attr.getValue();
	    int id=Integer.parseInt(idstr);
	    List elements=root.elements();
	    Element e1=(Element) elements.get(0);
	    Element e2=(Element) elements.get(1);
	    Element e3=(Element) elements.get(2);
	    String nom=e1.getText();
	    String prenom=e2.getText();
	    int age=Integer.parseInt(e3.getText());
	    Marin m=new Marin(id,nom,prenom,age);
	    return m;
		
	}
}
