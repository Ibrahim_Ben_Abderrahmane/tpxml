package org.Ben_Abderrahmane.CoursXML.tps.serie1.exo3;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Node;
import org.xml.sax.Attributes;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		SAXParserFactory spf=SAXParserFactory.newInstance();
		spf.setValidating(true);
		spf.setNamespaceAware(true);
		
		
		DefaultHandler defaultHandler =  new DefaultHandler() {
			//la variable dont on stock le nombre de sous éléments 
			 int nbelement = 0;
			 
			//la variable dont en stock le nombre d'item trouvés
			 int Itemcpt=0;
		
			 //la chaîne qui va contenir le nom de la racine 
			 String racine;
			 
			 //un boolean qui nous permet de savoir si l'element est un item ou pas 
			 boolean status_item=false;
			 boolean status_cloud=false;
			 //une liste qui va contenir le contenu d'item chaîne de caractère uniquement
			 List<String> listItem=new ArrayList<String>();
			
			 List<String> listCloud=new ArrayList<String>();
			/**
			 * Action a realiser au debut du document
			 */
			 public  void startDocument() {
				 // méthode appelée par l'analyseur SAX sur le début du document
				 System.out.println("-------------------------------------------------------------------------------------------");	
				 System.out.println("Début du Document");	
				 System.out.println("-------------------------------------------------------------------------------------------");
			 }
			 /**
			  * Action à réaliser lors de la detection d'un nouvel element
			  * @param nom
			  * @param prenom
			  * @param attributes
			  */

			public  void startElement(String uri,String localName,String qName, Attributes attributes) {
		
			    //si le compteur nbelement = 0 alors on'est dans l'élément racine
			    if(nbelement==0){
			    	racine=qName;
			    	System.out.println("----------------\t      Handler1 Nom de la racine     \t ---------------");
			    	System.out.println("Racine = "+racine+"\n");
			    }
			    
			    //on test si qName=item l'élément qu'on cherche 
			    if(qName.equals("item")){
			    	Itemcpt++;
			    	status_item=true;
			    }
			    if(qName.equals("category")){
			    	status_cloud=true;
			    }
			    nbelement++;
			}
			/**
			 * Action à réaliser lors de la detection de la fin d'element
			 * @param name
			 */
			public void endElement(String uri,String localName, String qName)throws SAXException{
			
				if(qName.equals(racine)){
					System.out.println("---------------- \t Handler2 Nombre de Sous-elements\t ---------------");
					System.out.println("il ya "+nbelement+" Sous-éléments"+"\n");
					System.out.println("----------------   Handler3 Nombre d'élément item dans le fulx   ---------------");
					System.out.println("il ya "+Itemcpt+" item dans le document"+"\n");
					System.out.println("---------------- Handler4 les titre des éléments item de ce flux ---------------");
					System.out.println("il ya "+listItem.size()+" éléments de type item dans la liste"+"\n");
					for(Object o: listItem){
						System.out.println("item n° "+(listItem.indexOf(o)+1)+"\t-> "+o);
					}
					
					System.out.println("\n---------------- Handler5 toutes les catégories contenant Cloud ---------------");
					System.out.println("il ya "+listCloud.size()+" 	Catégories qui contiennent Cloud"+"\n");
					for(int i=0;i<listCloud.size();i++){
						System.out.println("Categorie n° "+(i+1)+"\t-> "+listCloud.get(i));
					}
				}
			}
			
			public void characters(char[] ch,int debut,int longueur){
				String donnees_item = new String(ch,debut,longueur).trim();
				String donnees_cloud = new String(ch,debut,longueur).trim();
				if(donnees_item.length()>0){
					if(status_item){
						listItem.add(donnees_item);
						status_item=false;
					}
				}
				if(donnees_cloud.length()>0){
					if(status_cloud){
						String str[]=donnees_cloud.split(" ");
						for(int i=0;i<str.length;i++)
							if(str[i].equals("Cloud"))
								listCloud.add(donnees_cloud);
						
						status_cloud=false;
					}
				}
			    
			}
			/**
			 * Action à réaliser lors de la fin du document XML
			 */
			public void endDocument(){
				System.out.println("-------------------------------------------------------------------------------------------");	
				System.out.println("Fin du document");
				System.out.println("-------------------------------------------------------------------------------------------");
			}
			
		};
		try {
			SAXParser p=spf.newSAXParser();
			InputStream is =RSSReader.read("https://www.voxxed.com/feed/");
			p.parse(is,defaultHandler);		
		} 
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		} 
		catch (SAXException e) {
			e.printStackTrace();
		}
		catch(Exception e){
			System.out.println("WARNING : Can not Create File");
			e.printStackTrace();
		}
	}
}
