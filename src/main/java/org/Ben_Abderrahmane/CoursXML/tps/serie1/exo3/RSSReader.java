package org.Ben_Abderrahmane.CoursXML.tps.serie1.exo3;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class RSSReader {
	
	
	/**
	 * 
	 * @param url
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static InputStream read(String url) throws ClientProtocolException, IOException{
		//ouverture d'un client
		HttpClient httpclient= HttpClientBuilder.create().build();
		
		//creation d'une methode http
		HttpGet get=new HttpGet(url);
		
		HttpResponse reponse= httpclient.execute(get);
		
		//invocation de la methode
		StatusLine statusLine=reponse.getStatusLine();
		int statusCode= statusLine.getStatusCode();
		if(statusCode==statusLine.getStatusCode()){
			System.err.println("Method failed : "+statusLine);
		}
		
		//lecture de la réponse dans un flux 
		InputStream response=reponse.getEntity().getContent();
		return response;
	}
}
