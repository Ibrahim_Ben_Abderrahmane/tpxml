package org.Ben_Abderrahmane.CoursXML.tps.serie1.exo1;

import java.io.File;

import org.dom4j.DocumentException;

public class Main {

	public static void main(String[] args) throws DocumentException {
		// TODO Auto-generated method stub

			Marin m=new Marin(1,"Ibrahim","Ben Abderrahmane",23);
			File file=XMLUtil.createNewFile("Marins.xml");
			XMLUtil.write(XMLUtil.serialize(m),file);
			try {
				XMLUtil.read(file);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Marin m2=XMLUtil.deserialize(XMLUtil.read(file));
			
			System.out.println(m2.toString());
	}

}
