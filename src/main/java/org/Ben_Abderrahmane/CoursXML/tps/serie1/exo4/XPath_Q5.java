package org.Ben_Abderrahmane.CoursXML.tps.serie1.exo4;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.Ben_Abderrahmane.CoursXML.tps.serie1.exo3.RSSReader;
import org.apache.http.client.ClientProtocolException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XPath_Q5 {

	public List<String> Question5() throws DocumentException, ClientProtocolException, IOException
	{
	
		List<String> liste=new ArrayList<String>();
		
		InputStream xpath =RSSReader.read("http://webservices.amazon.com/AWSECommerceService/AWSECommerceService.wsdl");	
		
		SAXReader sax=new SAXReader ();
		
		Document doc=sax.read(xpath);
		
		List<Element> liste1 = doc.selectNodes("//*[name()='message']");
		
		for(Element elem : liste1)
			liste.add(elem.attribute("name").getValue());
		
		return liste;
		
	}
}
