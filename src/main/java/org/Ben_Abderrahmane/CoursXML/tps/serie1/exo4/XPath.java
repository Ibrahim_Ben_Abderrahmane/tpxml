package org.Ben_Abderrahmane.CoursXML.tps.serie1.exo4;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.xpath.XPathFactory;

import org.Ben_Abderrahmane.CoursXML.tps.serie1.exo3.RSSReader;
import org.apache.http.client.ClientProtocolException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XPath {

	public static void main(String[] args) throws DocumentException, ClientProtocolException, IOException {
		System.out.println("----------------------------------------------------------------------------------------------------------------------");		
		System.out.println("Q1: localiser le fichier WSDL des Web Services de Amazon \n");
		System.out.println("Saisissez l'url suivante sur votre navigateur:");
		System.out.println("http://docs.aws.amazon.com/AWSECommerceService/latest/DG/Welcome.html \n");
		System.out.println("A gauche de votre écran dans la zone Product Advertising API cliquez sur -> Programming Guide");
		System.out.println("Un tableau de deux colonnes va apparaître... ");
		System.out.println("Cliquez sur la section -> E-Commerce and Web Services\n");
		System.out.println("A gauche de votre écran cliquez sur -> What Is a WSDL?");
		System.out.println("Puis WSDL Location...");
		System.out.println("Une page avec un lien direct vers le fichier WSDL s'ouvrira!!!\n");
		System.out.println("WSDL Is Located at -> http://webservices.amazon.com/AWSECommerceService/AWSECommerceService.wsdl");
		System.out.println("----------------------------------------------------------------------------------------------------------------------");
		System.out.println("Q2:Quel est le neud Racine de ce document XML? Quel est son espace de nom par défaut?\n");
		System.out.println("Racine -> definitions");
		System.out.println("Espace de nom par defaut -> xmlns="+"http://schemas.xmlsoap.org/wsdl/\n");
		System.out.println("----------------------------------------------------------------------------------------------------------------------");
		System.out.println("Q3: Quel autre espace de nom propre à Amazon est-il défini dans le document? à quel préfixe est-il associé?\n");
		System.out.println("l'espace de nom propre à Amazon est -> targetNamespace="+"http://webservices.amazon.com/AWSECommerceService/2011-08-01");
		System.out.println("Associé au préfixe"+" tns"+" -> xmlns:tns="+"http://webservices.amazon.com/AWSECommerceService/2011-08-01");
		System.out.println("----------------------------------------------------------------------------------------------------------------------");
		
		//Dans le cas de cet exercice j'ai sauvegardé WSDL dans mon répértoir de travail		
		File xpath= new File("AWSECommerceService.wsdl.xml");
		
		SAXReader sax=new SAXReader ();
		Document doc=sax.read(xpath);
		Element elementRacine= doc.getRootElement();

		
		System.out.println("\nRacine ->" + elementRacine.getName()+"\n");
		
		System.out.println("*****************************************************\n");
		System.out.println("Q4-a : Le nombre de sous element de "+" types/schema "+"est : ");
		XPath_Q4 handler1 =new XPath_Q4();
		System.out.println("IL ya "+handler1.Question4a()+" Sous-éléments\n");	
		System.out.println("*****************************************************\n");
		
		System.out.println("Q4-b : liste des valeurs des attributes names des sous elements de"+" types/schema sont"+" : ");
		List<String> liste_attr_name=handler1.Question4b();
		
		for(String name : liste_attr_name)
			System.out.println(name);
		
		
		System.out.println("----------------------------------------------------------------------------------------------------------------------");
		System.out.println("Q5 : liste des valeurs des attributes name de l'element message est : \n");
		
		XPath_Q5 handler2 =new XPath_Q5();
		List<String> liste_attr_name_message=handler2.Question5();
		
		for(String name : liste_attr_name_message)
			System.out.println(name);
	
		
		/**********************   partie handler  *******************/
		SAXParserFactory spf=SAXParserFactory.newInstance();
		spf.setValidating(true);
		spf.setNamespaceAware(true);
		
		DefaultHandler defaultHandler =  new DefaultHandler() {
			String racine;
			int nbelement=0,cptschema=0,cpt=0;
			boolean status_types_shema=false,status_schema=false;
			List<String> nom = new ArrayList<String>();

			/**
			 * Action a realiser au debut du document
			 */
			 public  void startDocument() {
				 // méthode appelée par l'analyseur SAX sur le début du document
				 System.out.println("-------------------------------------------------------------------------------------------");	
				 System.out.println("Début du Document");	
				 System.out.println("-------------------------------------------------------------------------------------------");
			 }
		
			public  void startElement(String uri,String localName,String qName, Attributes attributes) {
			 //si le compteur nbelement = 0 alors on'est dans l'élément racine
			    if(nbelement==0){
			    	racine=qName;
			    	System.out.println("----------------\t      Handler1 Nom de la racine     \t ---------------");
			    	System.out.println("Racine = "+racine+"\n");
			    	System.out.println("TargetNamespace= "+attributes.getValue(0)+"\n");
			        
			    }
			    nbelement++;
			    cpt++;
			    if(localName.equals("types")){
			    	status_types_shema=true;
			    }
			    if(status_types_shema && cpt==4){
			    	cptschema++;
			    		
			    }
				if(qName.equals("message"))
				{
						if(attributes!= null)
						{
							for(int i=0;i<attributes.getLength();i++)
							{
								System.out.println(attributes.getValue(i));
								nom.add(attributes.getValue(i));
							}
						}
				}
			}
			/**
			 * Action à réaliser lors de la detection de la fin d'element
			 * @param name
			 */
			public void endElement(String uri,String localName, String qName)throws SAXException{
				cpt--;
				if (localName.equals("types"))
					status_types_shema=false;
				
				
			}
			/**
			 * Action à réaliser lors de la fin du document XML
			 */
			public void endDocument(){
				System.out.println("-------------------------------------------------------------------------------------------");	
				System.out.println("le nombre types/schema est : "+cptschema);
				System.out.println("Fin du document");
				System.out.println("-------------------------------------------------------------------------------------------");
			}
			
		};
		
		try {
			SAXParser p=spf.newSAXParser();
			InputStream is =RSSReader.read("http://webservices.amazon.com/AWSECommerceService/AWSECommerceService.wsdl");
			p.parse(is,defaultHandler);		
		} 
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		} 
		catch (SAXException e) {
			e.printStackTrace();
		}
		catch(Exception e){
			System.out.println("WARNING : Can not Create File");
			e.printStackTrace();
		}
		
	}
}