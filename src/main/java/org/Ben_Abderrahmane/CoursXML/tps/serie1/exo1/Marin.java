package org.Ben_Abderrahmane.CoursXML.tps.serie1.exo1;

public class Marin {

	private long id;
	private String Nom,Prenom;
	private int Age;
	
	
	
	@Override
	public String toString() {
		return "Marin [id=" + id + ", Nom=" + Nom + ", Prenom=" + Prenom + ", Age=" + Age + "]";
	}
	public Marin(long id, String nom, String prenom, int age) {
		super();
		this.id = id;
		Nom = nom;
		Prenom = prenom;
		Age = age;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getPrenom() {
		return Prenom;
	}
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	public int getAge() {
		return Age;
	}
	public void setAge(int age) {
		Age = age;
	}
	
}
